<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/about/{nama}',function($nama){
return "halo$nama";
});

Route ::get('/home','HomeController@home');

Route ::get('/register','AuthController@register');

Route :: post('/welcome','AuthController@welcome');

Route :: get('/master', function(){

    return view('adminlte.master');
});

route :: get('/table',function(){
return view ('adminlte.view.table');
});

Route :: get('/data-table',function(){

    return view ('adminlte.view.data-table');
});

Route :: get('/cast/create','PostController@create');

Route :: post('/cast','PostController@store');

Route :: get('/index','PostController@index');

Route :: get('/cast/{id}','PostController@show');

Route :: get('/cast/{id}/edit','PostController@edit');

Route :: put('cast/{id}','PostController@update');

Route :: delete('cast/{id}','PostController@destroy');