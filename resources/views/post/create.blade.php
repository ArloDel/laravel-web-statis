@extends('adminlte.master')

@section('content')
<div class = "ml-3 mt-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">form casting</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/cast" method="POST">
              @csrf
              
            
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value= "{{old('nama','')}}" placeholder="masukkan nama" required>
                    @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="umur">umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" value = "{{old('umur','')}}" placeholder="masukkan umur" required>
                    @error('umur')
                            <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="bio">bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" value = "{{old('bio','')}}" placeholder="masukkan bio" required>
                    @error('bio')
                            <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">create</button>
                </div>
              </form>
            </div>

</div>

@endsection