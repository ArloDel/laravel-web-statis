@extends('adminlte.master')

@section('content')
<div>
<div class="card-body">
@if(session('success'))
<div class = "alert alert-success">
{{session('success')}}
</div>
@endif
<a class ="btn btn-primary mb-2" href="/cast/create">create new cast</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>nama</th>
                      <th>umur</th>
                      <th>bio</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($casts as $key => $value)
                    <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->umur}}</td>
                    <td>{{$value->bio}}</td>

                    <td style="display: flex;">
                    <a href="cast/{{$value->id}}" class ="btn btn-primary">show</a>
                    <a href="cast/{{$value->id}}/edit" class ="btn btn-default ml-2">edit</a>
                    <form action="cast/{{$value->id}}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger ml-2">
                    </form>
                    </td>
                    
                    </tr>

                    @endforeach
                   
                  </tbody>
                </table>
              </div>
</div>

@endsection