<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\cast;

class PostController extends Controller
{
    public function create() {
        return view('post.create');
    }

    public function store(Request $request){
      //dd($request->all());
      $request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio'  => 'required'
    ]);
      
      /*$query = DB :: table('cast')-> insert([
          "nama" => $request["nama"],
          "umur" => $request["umur"],
          "bio" => $request["bio"]
          ]);*/
      $cast = new cast;
      $cast->nama = $request["nama"];
      $cast->umur = $request["umur"];
      $cast->bio = $request["bio"];
      $cast->save();    
      

      return redirect('/index')-> with('success','post berhasil disimpan !'); 
    }

    public function index(){

     //$casts = DB :: table('cast')-> get();
     $casts = cast :: all();
     //dd($index); 
        return view('post.index', compact('casts'));
    }

    public function show($id){
        //$cast = DB :: table('cast')->where('id',$id)->first();
        $cast = cast :: find($id);
        //dd($post);
        return view('post.show',compact('cast'));
    }

    public function edit ($id){
        //$casts = DB :: table('cast')->where('id',$id)->first();
        $casts = cast :: find($id);
        return view('post.edit',compact('casts'));
    }

    public function update($id,Request $request){

        /*$query = DB :: table('cast')
                        ->where('id',$id)
                        ->update([
                            'nama' => $request["nama"],
                            'umur' => $request["umur"],
                            'bio' => $request["bio"]
                        ]);*/
        $update = cast :: where('id',$id)->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
        ]);

        return redirect('/index')-> with('success','berhasil di update !');                
    }
    public function destroy($id){
        //DB::table('cast')->where('id',$id)->delete();
        cast :: destroy($id);
        return redirect('/index')-> with('success','berhasil di Hapus !');     
    }
}
