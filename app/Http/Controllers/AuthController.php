<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('register');
    }

    public function welcome(Request $request){
        $FirstName = $request["fn"];
        $LastName = $request["ln"];
        return view ('greeting', compact('FirstName','LastName'));
    }
}
